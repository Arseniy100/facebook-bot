# -*- coding: utf-8 -*-
"""
Created on Fri May  3 20:29:32 2019

@author: Арсений HP-15
"""

import requests
import requests.auth
from requests_toolbelt import MultipartEncoder
import json
from numpy.random import randint as np_random_randint

FACEBOOK_GRAPH_URL = 'https://graph.facebook.com/v3.3/me/'


class Bot(object):

    def __init__(self, access_token, api_url=FACEBOOK_GRAPH_URL):
        self.access_token = access_token
        self.api_url = api_url
        self.themes = [
            'dogs', 'cats', 'star wars', 'memes', 'game', 'panda', 'lotr',
            'lord of the rings', 'game of thrones', 'sherlock', 'lol',
            'landscape', 'nature', 'photography', 'cute', 'mountains',
            'forest', 'meme', 'dog', 'astronomy', 'astrophotography'
            ]
        self.Bot_texts = {
                'hellos': [
                    'Привет!', 'Добрый день.', 'Здравствуй!',
                    'Hello!', 'Bonjour!'
                    ],
                'problem_400': 'Простите, скачанная картинка не посылается. \
Что-то не так. Попробуйте еще раз.',
                'sending': 'Сейчас я выберу и пришлю тебе что-нибудь \
интересное из Реддита по теме ',
                'introduce': '\
Привет. Я - бот, отправляющий картинки с Реддита. \
Если хочешь мемов, напиши мне слово мем, а потом тему, \
по которой хочешь увидеть картинку. Например: "мем star wars". \
Или просто "мем", а я сам подберу тему. \
Если смелый, то напиши: \
"Я хочу больше мемов. Я понимаю, что бот может начать посылать их мне \
без остановки и с повторами, и я беру ответственность за это на себя." \
Я пришлю случайное количество картинок по случайным темам. \
Возможно, я пришлю МНОГО мемов (а, возможно, 0). Будь готов. \
Если хочешь увидеть это сообщение еще раз, напиши мне "старт". \
Ты можешь ознакомиться с моей политикой конфиденциальности по ссылке \
https://chatbot57.pythonanywhere.com/politics ',
                'not_found': 'Интересных мемов не найдено. Видимо, по этой \
теме мало картинок и много текстовых постов. Увы!',
                'no_comprendo': 'Ты сказал: "{}". Я такого не понимаю. \
Попробуй сказать что-то другое. Если хочешь узнать, что я могу, напиши \
"старт" или "start" (без кавычек).'}

        self.User_texts = {
            'agree': 'я хочу больше мемов. я понимаю, что бот может начать \
посылать их мне без остановки и с повторами, \
и я беру ответственность за это на себя.',
            'hellos': [
                'hello', 'hi', 'привет', 'даров', 'ку', 'приветик',
                'привет бот', 'привет, бот',
                'здравствуй', 'здравствуйте', 'прив', 'хай'
                ],
            'want_mem': ['мем', 'meme'],
            'start': [
                'start', 'команды', 'ты кто?', 'menu',
                'что ты умеешь?', 'старт', 'get started', 'help', 'information'
                ]
            }

    def send_text_message(self, psid, message, messaging_type='RESPONSE'):
        _headers = {'Content-Type': 'application/json'}

        _data = {
                'messaging_type': messaging_type,
                'recipient': {'id': psid},
                'message': {'text': message}}

        _params = {'access_token': self.access_token}
        _api_url = self.api_url + 'messages'
        _response = requests.post(
            _api_url,
            headers=_headers,
            params=_params,
            data=json.dumps(_data)
            )
        print(_response.content)

    def send_image(self, psid, image_url, messaging_type="RESPONSE"):
        _data = {
            # encode nested json to avoid errors
            # during multipart encoding process
            'recipient': json.dumps({
                'id': psid
            }),
            # encode nested json to avoid errors
            # during multipart encoding process
            'message': json.dumps({
                'attachment': {
                    'type': 'image',
                    'payload': {  # "is_reusable": True,
                        'url': image_url
                        }
                }
            })
        }
        _params = {
            'access_token': self.access_token
        }
        _api_url = self.api_url + 'messages'
        _multipart_data = MultipartEncoder(_data)
        _multipart_header = {
            'Content-Type': _multipart_data.content_type
        }
        _data = requests.post(
            _api_url, headers=_multipart_header,
            params=_params,
            data=_multipart_data
            )
        print(_data.status_code, _data.content)

        if _data.status_code == 400:
            print('problem with the url ', image_url)
            self.send_text_message(
                psid,
                self.Bot_texts['problem_400'],
                messaging_type='RESPONSE'
                )

    def send_memes(self, user_id, theme, limit=5):
        print('sending memes')
        self.send_text_message(user_id,  self.Bot_texts['sending']+theme)
#        connecting to reddit
        _client_auth = requests.auth.HTTPBasicAuth(
            '???',
            '??????'
            )
        _post_data = {
            "grant_type": "password",
            "username": "???",
            "password": "???"
            }

        headers = {"User-Agent": "py2/0.1 by Arseniy100"}
        response = requests.post("https://www.reddit.com/api/v1/access_token",
                                 auth=_client_auth,
                                 data=_post_data,
                                 headers=headers)
        _data = response.json()
        print(_data)
        _access_token = _data['access_token']
        print('Authorised successfully!')

        _api_url = "https://oauth.reddit.com"
        _headers = {
            "Authorization": "bearer "+_access_token,
            "User-Agent": "py2/0.1 by Arseniy100"
            }

        _payload = {'q': theme, 'limit': limit}
        _response = requests.get(
            _api_url + '/subreddits/search',
            headers=_headers,
            params=_payload
            )
        _js = _response.json()
        print(_js.keys())

        _sr = []
        for i in range(_js['data']['dist']):
            _sr.append(_js['data']['children'][i]['data']['display_name'])
        print('sr: ', _sr)

        _payload = {'t': 'all', 'limit': 5}
        img_urls = []

        for s in _sr:
            _r = requests.get(
                _api_url + '/r/{}/top'.format(s),
                headers=_headers,
                params=_payload
                )
            _js = _r.json()
            for i in range(_js['data']['dist']):
                if _js['data']['children'][i]['data']['thumbnail'] == '':
                    continue
                img_urls.append(_js['data']['children'][i]['data']['url'])
                if img_urls[-1][-3:] != 'jpg' and img_urls[-1][-3:] != 'png':
                    img_urls.pop()
        return img_urls

    def answer(self, text, user_id):
        text = text.lower()
        if text in self.User_texts['hellos']:
            self.send_text_message(
                user_id, self.Bot_texts['hellos'][
                    np_random_randint(0, len(self.Bot_texts['hellos']))
                    ]
                    )
        elif text in self.User_texts['start']:
            self.send_text_message(user_id, self.Bot_texts['introduce'])
        elif text == 'спойлер':
            self.send_image(user_id, 'https://i.redd.it/kcr8pshpi5w21.png')
        elif len(text) >= 3 and text[0:3] in self.User_texts['want_mem']:
            if len(text) == 3:
                theme = self.themes[np_random_randint(0, len(self.themes))]
            else:
                theme = text[3:]
            img_urls = self.send_memes(user_id, theme)
            if len(img_urls) == 0:
                self.send_text_message(user_id, self.Bot_texts['not_found'])
            else:
                url = img_urls[np_random_randint(0, len(img_urls))]
                self.send_image(user_id, url)
                print('url ok: ', url)

        elif len(text) >= 4 and text[0:4] in self.User_texts['want_mem']:
            if len(text) == 4:
                theme = self.themes[np_random_randint(0, len(self.themes))]
            else:
                theme = text[4:]
            img_urls = self.send_memes(user_id, theme)
            if len(img_urls) == 0:
                self.send_text_message(user_id, self.Bot_texts['not_found'])
            else:
                url = img_urls[np_random_randint(0, len(img_urls))]
                self.send_image(user_id, url)
                print('url ok: ', url)

        elif text == self.User_texts['agree']:
            theme = self.themes[np_random_randint(0, len(self.themes))]
            img_urls = self.send_memes(
                user_id, theme, limit=np_random_randint(2, 15)
                )
            if len(img_urls) == 0:
                self.send_text_message(user_id, self.Bot_texts['not_found'])
            else:
                for url in img_urls[0:np_random_randint(0, len(img_urls))]:
                    self.send_image(user_id, url)
                    print('url ok: ', url)
        else:
            self.send_text_message(
                user_id, self.Bot_texts['no_comprendo'].format(text)
                )
